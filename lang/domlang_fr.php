<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'domaine_pour_langue' => 'URL pour la langue : @nom@ [@lang@]',
	'domlang_titre' => 'Domaines par secteur de langue',

	// T
	'titre_ndd_par_secteur_langue' => 'Noms de domaines par secteurs de langue',
	'titre_page_configurer_domlang' => 'Définir les URLs pour chaque secteur de langue',
);
